package junit;

import junit.framework.TestCase;
import pass.BitwiseAnd;

public class BitwiseAndTest extends TestCase{
    private BitwiseAnd bit_op;

    protected void setUp() throws Exception{
        super.setUp();
        bit_op = new BitwiseAnd();
    }

    protected void tearDown() throws Exception{
        super.tearDown();
    }

    public void testBitwiseAnd(){
        this.assertEquals(bit_op.bAnd(12,25),8);
        this.assertEquals(bit_op.bAnd(3,4),0);
        this.assertEquals(bit_op.bAnd(100,50),32);
    }
}