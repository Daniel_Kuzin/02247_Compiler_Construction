// Copyright 2013 Bill Campbell, Swami Iyer and Bahar Akbal-Delibas

package junit;

import java.io.File;
import junit.framework.TestCase;
import junit.framework.Test;
import junit.framework.TestSuite;
import pass.*;

/**
 * JUnit test suite for running the j-- programs in tests/pass.
 */

public class JMinusMinusTestRunner {

    public static Test suite() {
        TestSuite suite = new TestSuite();
        suite.addTestSuite(HelloWorldTest.class);
        suite.addTestSuite(FactorialTest.class);
        suite.addTestSuite(GCDTest.class);
        suite.addTestSuite(SeriesTest.class);
        suite.addTestSuite(ClassesTest.class);
        suite.addTestSuite(DivisionTest.class);     //Test added for division
        suite.addTestSuite(RemainderTest.class);    //Test added for remainder
        suite.addTestSuite(ShiftLeftTest.class);    //Test added for shift left operator
        suite.addTestSuite(ShiftRightTest.class);    //Test added for shift right operator
        suite.addTestSuite(UnsignedShiftRightTest.class);    //Test added for unsigned shift right operator
        suite.addTestSuite(BitwiseAndTest.class);    // Test added for Bitwise And
        suite.addTestSuite(BitwiseOrTest.class);    // Test added for Bitwise Or
        suite.addTestSuite(BitwiseXorTest.class);    // Test added for Bitwise Xor
        suite.addTestSuite(UnaryComplementTest.class);    // Test added for Unary Complement
        suite.addTestSuite(LogicalOrTest.class);    // Test added for Logical Or
        suite.addTestSuite(ConditionalExpTest.class);
        return suite;
    }

    /**
     * Runs the test suite using the textual runner.
     */

    public static void main(String[] args) {
        junit.textui.TestRunner.run(suite());
    }

}
