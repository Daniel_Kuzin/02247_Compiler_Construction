package junit;

import junit.framework.TestCase;
import pass.UnsignedShiftRight;

public class UnsignedShiftRightTest extends TestCase{
    private UnsignedShiftRight shift_op;

    protected void setUp() throws Exception{
        super.setUp();
        shift_op = new UnsignedShiftRight();
    }

    protected void tearDown() throws Exception{
        super.tearDown();
    }

    public void testUnsignedShiftRight(){
        this.assertEquals(shift_op.ur_shift(5,2),1);
        this.assertEquals(shift_op.ur_shift(5,-1),0);
        this.assertEquals(shift_op.ur_shift(-2,-1),1);
    }
}