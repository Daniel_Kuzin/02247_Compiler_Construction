package junit;

import junit.framework.TestCase;
import pass.ShiftLeft;

public class ShiftLeftTest extends TestCase{
    private ShiftLeft shift_op;

    protected void setUp() throws Exception{
        super.setUp();
        shift_op = new ShiftLeft();        
    }

    protected void tearDown()  throws Exception{
        super.tearDown();
    }

    public void testShiftLeft(){
        this.assertEquals(shift_op.l_shift(5,2),20);
        this.assertEquals(shift_op.l_shift(1,2),4);
        this.assertEquals(shift_op.l_shift(1,1),2);
    }
}