package junit;

import junit.framework.TestCase;
import pass.BitwiseOr;

public class BitwiseOrTest extends TestCase{
    private BitwiseOr bit_op;

    protected void setUp() throws Exception{
        super.setUp();
        bit_op = new BitwiseOr();
    }

    protected void tearDown() throws Exception{
        super.tearDown();
    }

    public void testBitwiseOr(){
        this.assertEquals(bit_op.bOr(100,50),118);
        this.assertEquals(bit_op.bOr(2,3),3);
        this.assertEquals(bit_op.bOr(5,10),15);
    }
}