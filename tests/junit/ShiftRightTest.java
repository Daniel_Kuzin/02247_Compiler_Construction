package junit;

import junit.framework.TestCase;
import pass.ShiftRight;

public class ShiftRightTest extends TestCase{
    private ShiftRight shift_op;

    protected void setUp() throws Exception{
        super.setUp();
        shift_op = new ShiftRight();
    }

    protected void tearDown() throws Exception{
        super.tearDown();
    }

    public void testShiftRight(){
        this.assertEquals(shift_op.r_shift(5,2),1);
        this.assertEquals(shift_op.r_shift(-5,2),-2);
        this.assertEquals(shift_op.r_shift(10,1),5);
    }
}