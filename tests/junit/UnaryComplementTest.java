package junit;

import junit.framework.TestCase;
import pass.UnaryComplement;

public class UnaryComplementTest extends TestCase{
    private UnaryComplement unComplement;

    protected void setUp() throws Exception{
        super.setUp();
        unComplement = new UnaryComplement();
    }

    protected void tearDown() throws Exception{
        super.tearDown();
    }

    public void testUnaryComplement(){
        this.assertEquals(unComplement.unComplement(8),-9);
        this.assertEquals(unComplement.unComplement(~8),8);
        this.assertEquals(unComplement.unComplement(1),-2);
        this.assertEquals(unComplement.unComplement(-3),2);
    }
}