package junit;

import junit.framework.TestCase;
import pass.BitwiseXor;

public class BitwiseXorTest extends TestCase{
    private BitwiseXor bit_op;

    protected void setUp() throws Exception{
        super.setUp();
        bit_op = new BitwiseXor();
    }

    protected void tearDown() throws Exception{
        super.tearDown();
    }

    public void testBitwiseXor(){
        this.assertEquals(bit_op.bXor(100,50),86);
        this.assertEquals(bit_op.bXor(1,2),3);
        this.assertEquals(bit_op.bXor(5,10),15);
    }
} 