package junit;
import pass.ConditionalExp;
import junit.framework.TestCase;

public class ConditionalExpTest extends TestCase{
    private ConditionalExp condExp;
    protected void setUp() throws Exception{
        super.setUp();
        condExp = new ConditionalExp();
    }

    protected void tearDown() throws Exception{
        super.tearDown();
    }

    public void testConditionalExpression(){
        assertEquals(condExp.condExp(false), 3 );
        assertEquals(condExp.condExp(true), 2);
    }    
}
