package junit;

import junit.framework.TestCase;
import pass.Remainder;

public class RemainderTest extends TestCase{
    private Remainder remainder;

    protected void setUp() throws Exception{
        super.setUp();
        remainder = new Remainder();
    }

    protected void tearDown() throws Exception{
        super.tearDown();
    }

    public void testRemainder(){
        this.assertEquals(remainder.remainder(3,2),1);
        this.assertEquals(remainder.remainder(4,4),0);
        this.assertEquals(remainder.remainder(10,2),0);
    }
}