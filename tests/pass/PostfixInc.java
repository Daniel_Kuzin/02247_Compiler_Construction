package pass;

import java.lang.System;

public class PostfixInc{
    public static void main(String[] args){
       // The Post ++ Operator gives an error when it gets type double
       // double z = 4.0;
       // double x = z++;

        int z = 4;
        System.out.println("The value before the operator is applied: " + z++);
        int x = z++;
        System.out.println("The value after the operator is applied: " + x);
        
    }
}