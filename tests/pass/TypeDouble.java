package pass;

import java.lang.System;

public class TypeDouble{
    
    double add_doubles1(double a, double b)
    {
        return a + b;
    }

    void add_doubles2(double a, double b)
    {
        System.out.println(a + b);
    }

    public static void main(String[] args){
        double z = 1.21;
        double x = 2.21;
        System.out.println(x - z);
        System.out.println(x + z);
        System.out.println(x * z);

        TypeDouble type_tool = new TypeDouble();
        double result = type_tool.add_doubles1(x, z);
        System.out.println(result);
        
        type_tool.add_doubles2(x, z);
    }
}
