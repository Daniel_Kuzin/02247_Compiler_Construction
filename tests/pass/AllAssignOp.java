package pass;
import java.lang.System;

public class AllAssignOp {
    public static void main(String[] args) {
        
        int a = 3;
        int b = 2;
        a+=b;
        System.out.println(a);
        a-=b;
        System.out.println(a);
        a*=b;
        System.out.println(a);
        a/=b;
        System.out.println(a);
        a%=b;
        System.out.println(a);
        
        double x = 3.54;
        double y = 2.82;
        x+=y;
        System.out.println(x);
        x-=y;
        System.out.println(x);
        x*=y;
        System.out.println(x);
        x/=y;
        System.out.println(x);
        x%=y;
        System.out.println(x);
    }
}
