package pass;

public class BitwiseXor{
    public int bXor(int x, int y){
        return x ^ y;
    }
}