// Copyright 2013 Bill Campbell, Swami Iyer and Bahar Akbal-Delibas

package pass;

import java.lang.System;

class A {

    public static String a = "Hello";

}

class B {

    public String b = "World!";

}

class C {

    public String c = "\n";

}

public class Classes extends C{

    public static String message() {
        return A.a + ", " + (new B()).b;
    }

    public static void main(String[] args) {
        System.out.println(Classes.message());
    }

}

