package pass;

import java.lang.System;

public class ForStatement_Form1 {
    public static void main(String[] args) {
        int i;
        int j;
        System.out.println("Test 1");
        for(i=1;i<=5;i++){
            System.out.println(i);
        }

        System.out.println("Test 2");
        for(j=10;j>1;j--){
            System.out.println(j);
        }

        System.out.println("Test 3");
        for(j=10,i=1;j>i;j--,i++){
            System.out.println(j*i);
        }
    }
}
