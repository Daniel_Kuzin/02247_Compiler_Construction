package pass;

import java.lang.System;

public class PrefixDec{
    public static void main(String[] args){
        // The -- Pre Operator gives an error when it gets type double
        // double x = 4.0;
        // double z = --x;

        int x = 4;
        int z = --x;
        
        System.out.println(z);
    }
}