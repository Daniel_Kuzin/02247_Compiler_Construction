package pass;

import java.lang.System;

public class TryCatch {
    public static void main(String[] args) {
        System.out.println("Text");
        try {
            System.out.println("Try");
            int[] myNumbers = {1, 2, 3};
            System.out.println(myNumbers[10]);
        } catch (Exception e) {
            System.out.println("Catch");
        } finally {
            System.out.println("Finally");
        }
    }
}
