package pass;

import java.lang.ArithmeticException ;
import java.lang.System;

public class Throw {

    public static int f() throws ArithmeticException{
        System.out.println("Random Text");
        throw new ArithmeticException("/ by zero");
        return 1;
    }
    public static void main(String[] args) {
        f();
    }
}
