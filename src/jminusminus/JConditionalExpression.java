package jminusminus;

import static jminusminus.CLConstants.*;

public class JConditionalExpression extends JExpression{
    //  condition
    // trueExpression
    //  falseExpression
    // @param line

    private JExpression condition;
    private JExpression trueExpression;
    private JExpression falseExpression;

    public JConditionalExpression(int line, JExpression condition, JExpression trueExpression, JExpression falseExpression){
        super(line);
        this.trueExpression = trueExpression;
        this.falseExpression = falseExpression;
        this.condition = condition;
    }

    public JExpression analyze(Context context){
        condition =  condition.analyze(context);
        condition.type().mustMatchExpected(line(), Type.BOOLEAN);
        trueExpression = (JExpression) trueExpression.analyze(context);
        falseExpression = (JExpression) falseExpression.analyze(context);
        type = Type.INT;
        return this;

    }
    public void codegen(CLEmitter output){
        String falseLabel = output.createLabel();
        String endLabel = output.createLabel();
        condition.codegen(output, falseLabel, false);
        trueExpression.codegen(output);
        if (falseExpression != null) {
            output.addBranchInstruction(GOTO, endLabel);
        }
        output.addLabel(falseLabel);
        if (falseExpression != null) {
            falseExpression.codegen(output);
            output.addLabel(endLabel);
        }
    }
    
    public void writeToStdOut(PrettyPrinter p) {
        p.printf("<JConditionalExpression line=\"%d\">\n", line());
        p.indentRight();
        p.printf("<Condition>\n");
        p.indentRight();
        condition.writeToStdOut(p);
        p.indentLeft();
        p.printf("</Condition>\n");
        p.printf("<TrueExpression>\n");
        p.indentRight();
        trueExpression.writeToStdOut(p);
        p.indentLeft();
        p.printf("</TrueExpression>\n");
        p.printf("<FalseExpression>\n");
        p.indentRight();
        falseExpression.writeToStdOut(p);
        p.indentLeft();
        p.printf("</FalseExpression>\n");
        p.indentLeft();
        p.printf("</JConditionalExpression>\n");
    }
}
