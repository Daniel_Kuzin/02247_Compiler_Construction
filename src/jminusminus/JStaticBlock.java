package jminusminus;

public class JStaticBlock extends JAST implements JMember {

    /** Method body. */
    protected JBlock body;

    public JStaticBlock(int line, JBlock body) {
        super(line);
        this.body = body;
    }

    public void preAnalyze(Context context, CLEmitter partial) {
    }

    public JAST analyze(Context context) {
        body.analyze(context);
        return this;
    }

    public void codegen(CLEmitter output) {
        body.codegen(output);
    }

    public void writeToStdOut(PrettyPrinter p) {
        p.printf("<JStaticBlock line=\"%d\">\n", this.line);
        p.indentRight();
        p.println("<Modifiers>");
        p.indentRight();
        p.printf("<Modifier name=\"static\"/>\n");
        p.indentLeft();
        p.println("</Modifiers>");
        body.writeToStdOut(p);
        p.indentLeft();
        p.printf("</JStaticBlock>\n");
    }
}