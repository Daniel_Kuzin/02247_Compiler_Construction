package jminusminus;
import static jminusminus.CLConstants.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.concurrent.PriorityBlockingQueue;

class JForStatement extends JStatement{
    
    private List<JStatementExpression> forInit;
    private ArrayList<JVariableDeclarator> fInit;
    private JExpression expression;
    private JStatement statement;
    private ArrayList<JStatementExpression> forUpdate;
    private String form;
 
    public JForStatement(int line, ArrayList<JVariableDeclarator> fInit, JExpression expression, 
    ArrayList<JStatementExpression> forUpdate, JStatement statement, String form) {
        super(line);
        this.fInit = fInit;
        this.expression = expression;
        this.forUpdate = forUpdate;
        this.statement = statement;
        this.form = form;
    }
 
    public JForStatement(int line, List<JStatementExpression> forInit, JExpression expression, 
    ArrayList<JStatementExpression> forUpdate, JStatement statement, String form) {
        super(line);
        this.forInit = forInit;
        this.expression = expression;
        this.statement = statement;
        this.forUpdate = forUpdate;
        this.form = form;
    }
 
    public JStatement analyze(Context context) {
        LocalContext for_context = new LocalContext(context); 
        ArrayList<JStatement> initializations = new ArrayList<JStatement>();
        Type form2_type = null;
        int offset = ((LocalContext) context).offset();
        if(forInit!=null){
            for(JStatementExpression se: forInit){
                se = (JStatementExpression) se.analyze(context);
            }
        }
        if(fInit!=null){
            for(JVariableDeclarator vd: fInit){
                LocalVariableDefn loc_variable = new LocalVariableDefn(vd.type().resolve(context), offset);
                if(this.form.equals("form1")){
                    if(vd.initializer()!=null){        
                        for_context.addEntry(vd.line(), vd.name(), loc_variable);
                        JAssignOp assignment = new JAssignOp(vd.line(), new JVariable(vd.line(), vd.name()), vd.initializer());
                        assignment.isStatementExpression = true;
                        JStatementExpression local_assignment = new JStatementExpression(vd.line(), assignment);
                        local_assignment.analyze(for_context);
                        
                    } else{
                        System.out.println("Variable " + vd.name() + " has not been initialized.");
                    }
                } else if(this.form.equals("form2")){
                    //Do some magic
                    for_context.addEntry(vd.line(), vd.name(), new LocalVariableDefn(vd.type(), for_context.nextOffset()));
                    vd.analyze(for_context);
                }
            }
        }
        
        if(expression!=null){
            if(this.form.equals("form1")){
                expression = expression.analyze(for_context);
                expression.type().mustMatchExpected(line(), Type.BOOLEAN); 
            } else {
                expression = expression.analyze(for_context);
                expression.type().mustMatchExpected(line(), Type.ANY);
            }
        }
        
        if(forUpdate!=null){
            for(JStatementExpression se: forUpdate){
                se = (JStatementExpression) se.analyze(for_context);
            }
        }
            
        if(statement!=null){
            statement = (JStatement) statement.analyze(for_context);
        }
        
        return this;
    }

    
    public void codegen(CLEmitter output) {
        String test = output.createLabel();
        String out = output.createLabel();

        if(this.form.equals("form1")){
            if(forInit!=null){
                for(JStatementExpression se: forInit){
                    se.codegen(output);
                }
            }
            if(fInit!=null){
                for(JVariableDeclarator vd: fInit){
                    
                    vd.codegen(output);
                }
            }
            
            output.addLabel(test);
            expression.codegen(output, out, false);
            
            statement.codegen(output);
            if(forUpdate!=null){
                for(JStatementExpression se: forUpdate){
                    se.codegen(output);
                }
            }

            output.addBranchInstruction(GOTO, test);
     
            output.addLabel(out);
        } else if(this.form.equals("form2")){
            
            
            output.addLabel(test);
            statement.codegen(output);

            output.addBranchInstruction(GOTO, test);
     
            output.addLabel(out);
        }
        
    }
    
    public void writeToStdOut(PrettyPrinter p) {
        p.printf("<JForStatement line=\"%d\">\n", line());
        p.indentRight();
        p.printf("<JForInit>\n");
        p.indentRight();
        if(forInit!=null){
            for(JStatementExpression s: forInit){
                s.writeToStdOut(p);
            }
        }
        if(fInit!=null){
            for(JVariableDeclarator s: fInit){
                s.writeToStdOut(p);
            }
        }           
        p.indentLeft();
        p.printf("</JForInit>\n");
        p.printf("<JExpression>\n");
        p.indentRight();
        if (expression!=null){
            expression.writeToStdOut(p);
        }
        p.indentLeft();
        p.print("</JExpression>\n");
        p.printf("<JForUpdate>\n");
        p.indentRight();
        if(forUpdate!=null){
            p.indentRight();
            for(JStatementExpression c : forUpdate){
                c.writeToStdOut(p);
            }
            p.indentLeft();
        }
        p.indentLeft();
        p.printf("</JForUpdate>\n");
        p.printf("<Body>\n");
        p.indentRight();
        statement.writeToStdOut(p);
        p.indentLeft();
        p.printf("</Body>\n");
        p.indentLeft();
        p.printf("</JForStatement>\n");
    }
    
}