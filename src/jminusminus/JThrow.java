package jminusminus;

import jminusminus.CLEmitter;
import static jminusminus.CLConstants.*;

class JThrow extends JStatement {

    private JExpression condition;

    public JThrow(int line, JExpression condition) {
        super(line);
        this.condition = condition;
       
    }

    public JStatement analyze(Context context) {
    	condition = condition.analyze(context);
        return this;
    }

    public void codegen(CLEmitter output) {
        condition.codegen(output);
    	output.addNoArgInstruction(ATHROW);
    }

    public void writeToStdOut(PrettyPrinter p) {
        p.printf("<JThrow line=\"%d\">\n", line());
        p.indentRight();
        p.printf("<TestExpression>\n");
        p.indentRight();
        condition.writeToStdOut(p);
        p.indentLeft();
        p.printf("</TestExpression>\n");
        
        p.printf("</JThrow>\n");
    }

}
