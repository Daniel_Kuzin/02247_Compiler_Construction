package jminusminus;

import static jminusminus.CLConstants.*;

import java.util.ArrayList;

public class JTryCatch extends JStatement {

	private JBlock tryBlock;
	private ArrayList<JFormalParameter> exceptions;
	private ArrayList<JBlock> catchBlocks;
	private JBlock finallyBlock;
	private LocalContext context;

	public JTryCatch(int line, JBlock tryBlock, ArrayList<JFormalParameter> exceptions, ArrayList<JBlock> catchBlocks,
			JBlock finallyBlock) {
		super(line);
		this.tryBlock = tryBlock;
		this.exceptions = exceptions;
		this.catchBlocks = catchBlocks;
		this.finallyBlock = finallyBlock;
	}

	public JStatement analyze(Context context) {
		this.context = new LocalContext(context);
		tryBlock.analyze(this.context);
		
		//Check if exception is allready been caught
		for (JFormalParameter param1 : exceptions) {
			for (JFormalParameter param2 : exceptions) {
				if (param1 != param2 && param1.type() == param2.type()) {
					JAST.compilationUnit.reportSemanticError(line,
		                    "%s has allready been caught", param2.type().toString());
				}
			}
		}
		
		
		for (int i = 0; i < exceptions.size(); i++) {
			LocalVariableDefn defn = new LocalVariableDefn(exceptions.get(i).type(), this.context.nextOffset());
			defn.initialize();
			this.context.addEntry(exceptions.get(i).line(), exceptions.get(i).name(), defn);
			
			//Check for missing catch block
			if (catchBlocks.size() < (i + 1)) {
				JAST.compilationUnit.reportSemanticError(line,
	                    "Missing catch block");
			} 
			else {
				catchBlocks.get(i).analyze(this.context);
			}
		}
		
		finallyBlock.analyze(this.context);
		
		return this;
	}

	public void codegen(CLEmitter output) {
		tryBlock.codegen(output);

		// code for catch
		
		if (finallyBlock != null){
			finallyBlock.codegen(output);
		}
	}

	public void writeToStdOut(PrettyPrinter p) {
		p.printf("<JTryCatch line=\"%d\">\n", line());
		p.indentRight();
		p.printf("<TryBlock>\n");
		p.indentRight();
		tryBlock.writeToStdOut(p);
		p.indentLeft();
		p.printf("</TryBlock>\n");

		for (int i = 0; i < exceptions.size(); i++) {
			p.printf("<CatchParameter>\n");
			p.indentRight();
			exceptions.get(i).writeToStdOut(p);
			p.indentLeft();
			p.printf("</CatchParameter>\n");
			p.printf("<CatchBlock>\n");
			p.indentRight();
			catchBlocks.get(i).writeToStdOut(p);
			p.indentLeft();
			p.printf("</CatchBlock>\n");
		}

		if (finallyBlock != null) {
			p.printf("<FinallyBlock>\n");
			p.indentRight();
			finallyBlock.writeToStdOut(p);
			p.indentLeft();
			p.printf("</FinallyBlock>\n");
		}
		p.indentLeft();
		p.printf("</JTryCatch>\n");
	}

}
