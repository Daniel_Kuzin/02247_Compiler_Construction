package jminusminus;

public class JInitializationBlock extends JAST implements JMember {

    /** Method body. */
    protected JBlock body;

    public JInitializationBlock(int line, JBlock body) {
        super(line);
        this.body = body;
    }

    public void preAnalyze(Context context, CLEmitter partial) {
    }

    public JAST analyze(Context context) {
        body.analyze(context);
        return this;
    }

    public void codegen(CLEmitter output) {
        body.codegen(output);
    }

    public void writeToStdOut(PrettyPrinter p) {
        p.printf("<JInitializationBlock line=\"%d\">\n", this.line);
        p.indentRight();
        body.writeToStdOut(p);
        p.indentLeft();
        p.printf("</JInitializationBlock>\n");
    }
}